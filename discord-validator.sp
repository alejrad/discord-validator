/*===================================
=            Plugin Info            =
===================================*/

public Plugin myinfo = {
	name = "Discord validator",
	author = "Hamster Anfetoso & Lownine",
	description = "",
	version = "1.0",
	url = ""
};

Handle g_hDb = null; 											// SQL driver
char g_steamId[MAXPLAYERS + 1][32];

// Chat colors
#define DARKRED 0x02
#define LIMEGREEN 0x06

#include <sourcemod>
#include <colorvariables>
#include <string>
#include "discord-validator/sql.sp"
#include "discord-validator/verify.sp"


public void OnPluginStart() {
	connectDatabase();
	RegConsoleCmd("sm_verify", Command_Verify, "Verifies your discord account");
}

//Store player steam id on global variable
public void OnClientPutInServer(int client) {
	if (!IsFakeClient(client)){
		GetClientAuthId(client, AuthId_Steam2, g_steamId[client], MAX_NAME_LENGTH, true);
	}
}
