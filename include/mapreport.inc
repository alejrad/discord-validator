/**
 * Called when a player reports a map
 *
 * @param client     Player index.
 * @return           Plugin_Handled; to stop the player from searching.
 */
forward void OnMapReport(char type[32], int number, char reason[32], char map[128], char steamId[32]);
