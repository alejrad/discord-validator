public Action Command_Verify(int client, int args)
{
	char arg[128];
	if (args < 1)	{
		PrintToChat(client, "Pone el codigo que te dio el bot; uso: sm_verify codigo");
		return Plugin_Handled;
	}
	GetCmdArg(1, arg, sizeof(arg));
	verifyPlayer(client, arg);
	return Plugin_Handled;
}


public void verifyPlayer(int client, char[] code) {
	char szQuery[256];
	Format(szQuery, 256, sql_findByCode, g_steamId[client], code);

	Handle data = createPack(client, code);
	SQL_TQuery(g_hDb, codeValidCallback, szQuery, data, DBPrio_Low);
}

public void codeValidCallback(Handle owner, Handle hndl, const char[] error, any data) {
	ResetPack(data);
	int client = ReadPackCell(data);
	char code[128];
	ReadPackString(data, code, 128);

	if (hndl == null) {
		CPrintToChat(client, "%c[Daylights] Error, contacte a un administrador", DARKRED);
		return;
	}

	if (SQL_HasResultSet(hndl) && SQL_GetAffectedRows(hndl) > 0)
	{
		char szQuery[256];
		Format(szQuery, 256, sql_validate, g_steamId[client], code);
		SQL_TQuery(g_hDb, verifyCallback, szQuery, client, DBPrio_Low);
	} else {
		CPrintToChat(client, "%c[Daylights] El codigo ingresado es incorrecto.", DARKRED);
	}
}


public void verifyCallback(Handle owner, Handle hndl, const char[] error, any client) {
	if (hndl == null) {
		PrintToChat(client, "%c[Daylights] Error, contacte a un administrador", DARKRED);
		return;
	}
	CPrintToChat(client, "%c[Daylights] Exito! Linkea2 con discord", LIMEGREEN);
}


public Handle createPack(int client, char[] code) {
	Handle pack = CreateDataPack();
	WritePackCell(pack, client);
	WritePackString(pack, code);
	return pack;
}